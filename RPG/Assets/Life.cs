﻿using UnityEngine;
using System.Collections;

public class Life : MonoBehaviour, IDamagable {

	[SerializeField] float initialLife;
	public float lifePoints {get; private set;}

	void Start (){
		lifePoints = initialLife;
	}

	public void DoDamage (float damage) {
		lifePoints -= damage;
		if(lifePoints < 0){
			gameObject.SetActive (false);
		}
	}

}
