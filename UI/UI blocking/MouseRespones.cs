﻿using UnityEngine;
using System.Collections;

public class MouseRespones : MonoBehaviour {

	bool isRed;

	public void OnMouseUp ()
	{
		isRed = !isRed;
		if (isRed) {
			GetComponent<Renderer>().material.color = Color.red;
		} else {
			GetComponent<Renderer>().material.color = Color.white;
		}
	}
}
