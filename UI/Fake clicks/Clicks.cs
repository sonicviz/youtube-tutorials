﻿using UnityEngine;
using UnityEngine.UI;

public class Clicks : MonoBehaviour {

	public void Red (){
		GetComponent<Image>().color = Color.red;
	}

	public void Yellow (){
		GetComponent<Image>().color = Color.yellow;
	}

	public void Green (){
		GetComponent<Image>().color = Color.green;
	}

	public void Blue (){
		GetComponent<Image>().color = Color.blue;
	}
}
